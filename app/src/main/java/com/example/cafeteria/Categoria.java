package com.example.cafeteria;

import java.util.ArrayList;
import java.util.List;

public class Categoria {
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<String> getItensCategoria() {
        List<String> itens = new ArrayList<>();

        if(descricao.equalsIgnoreCase("Bebidas")){
            itens.add("Café");
            itens.add("Água");
            itens.add("Refrigerante");
        }else if(descricao.equalsIgnoreCase("lanches salgados")){
            itens.add("Pastel");
            itens.add("Empada");
            itens.add("Croquete");
        }else{
            itens.add("Croissant");
            itens.add("Cupcake");
            itens.add("Bolo");
        }
        return itens;
    }
}
